<?php

/**
 * @file
 * Form builders, form validation and submit callbacks.
 */

/**
 * Form builder. Allows administrator to add new or update an existing background stretcher.
 */
function bgstretcher_admin($form, &$form_state, $id = NULL, $action = NULL) {

  $edit = FALSE;
  if ($action == 'edit' && $id) {
    // Get the existing bgstretcher from DB
    $bgstretcher = bgstretcher_bgstretcher_get($id);
    $settings = $bgstretcher['settings'];
    $settings_js = $bgstretcher['settings']['js'];
    // Set boolean flag to indicate that we're on "edit" mode now.
    $edit = TRUE;
  }
  
  // We want to deal with nested form array
  $form['#tree'] = TRUE;
  
  $form['id'] = array(
    '#type' => 'value',
  '#value' => is_numeric($id) ? $id : NULL,
  );
  
  $form['id'] = array(
    '#type' => 'value',
    '#value' => is_numeric($id) ? $id : NULL,
  );
  
  // Main background stretcher settings
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $edit ? check_plain($bgstretcher['name']) : '',
    '#weight' => -10,
  );
  
  $form['css'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS ID'),
    '#required' => TRUE,
    '#weight' => -5,
    '#default_value' => $edit ? check_plain($bgstretcher['css']) : '',
    '#description' => t("bgStretcher plugin will automatically build structure for the images list in a DOM tree.
       This parameter is ID for the images holder.
        Try inspecting the tree with a FireBug to get an idea how it's constructed."),
  );
  
  // Additional settings under vertical tabs
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 0,
  );
  
  $form['settings']['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#collapsible' => FALSE,
    '#group' => 'additional_settings',
  );
  
  $form['settings']['source'] = array(
    '#type' => 'fieldset',
    '#title' => t('Source'),
    '#collapsible' => FALSE,
    '#group' => 'additional_settings',
  );
  
  $form['settings']['js'] = array(
    '#type' => 'fieldset',
    '#title' => t('JS settings'),
    '#collapsible' => FALSE,
    '#group' => 'additional_settings',
  );
  
  $form['settings']['visibility']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show on specific pages'),
    '#options' => array(t('All pages except those listed'), t('Only the listed pages')),
    '#default_value' => $edit ? check_plain($bgstretcher['settings']['visibility']['visibility']) : 0,
  );
  
  $form['settings']['visibility']['path'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#rows' => 5,
    '#default_value' => $edit ? $bgstretcher['settings']['visibility']['path'] : '',
    '#description' => t("Specify pages by using their paths. 
    Enter one path per line. The '*' character is a wildcard. 
    Example paths are %user for the current user's page and %user-wildcard for every user page. 
    %front is the front page.", array('%user' => 'user', '%user-wildcard' => 'user/*', '%front' => '<front>')),
  );
  
  // This container used as wrapper for ajax callback
  $form['settings']['source']['wrapper'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="bgstretcher-source-wrapper">',
    '#suffix' => '</div>',
  );
  
  // Currently supported the following slide sources
  // TODO: use a hook?
  $options = array();
  $options['upload'] = t('Uploaded images');
  $options['node'] = t('Nodes');
  $options['url'] = t('Url');
  
  // If Views module installed, we can also use a view as slides's source
  if (module_exists('views')) {
    $options['view'] = t('View');
  }
  
  $form['settings']['source']['wrapper']['source'] = array(
    '#type' => 'select',
    '#title' => t('Slides source'),
    '#options' => $options,
    '#default_value' => $edit ? check_plain($bgstretcher['settings']['source']['source']) : 'upload',
    '#ajax' => array(
      'callback' => 'bgstretcher_admin_ajax_callback',
      'wrapper' => 'bgstretcher-source-wrapper',
    ),
  );
  
  if (isset($form_state['values']['settings']['source']['wrapper']['source']) || $edit) {
    
    if ($edit) {
      if (!empty($form_state['values']['settings']['source']['wrapper']['source'])) {
        $source = $form_state['values']['settings']['source']['wrapper']['source'];
      }
      else {
        $source = $settings['source']['source'];
      }
    }
    else {
      $source = $form_state['values']['settings']['source']['wrapper']['source'];
    }
    
    if ($source == 'node') {
      $options = array();
      $instances = field_info_instances('node');
      $node_types = node_type_get_types();
      
      foreach ($instances as $node_type => $fields) {
        $options[$node_type] = check_plain($node_types[$node_type]->name);
      }
  
      if (!empty($options)) {
        $default_node_type = $edit ? $settings['source']['node']['type'] : NULL;
        $form['settings']['source']['wrapper']['node']['type'] = array(
          '#type' => 'select',
          '#title' => t('Node type'),
          '#required' => TRUE,
          '#options' => $options,
          '#default_value' => $default_node_type,
          '#ajax' => array(
            'callback' => 'bgstretcher_admin_ajax_callback',
            'wrapper' => 'bgstretcher-source-wrapper',
          ),
        );

        if ($edit) {
          if (!empty($form_state['values']['settings']['source']['wrapper']['node']['type'])) {
            $node_type = $form_state['values']['settings']['source']['wrapper']['node']['type'];
          }
          else {
            $node_type = $settings['source']['node']['type'];
          }
        }
        else {
          $node_type = $form_state['values']['settings']['source']['wrapper']['node']['type'];
        }
    
        $options = array();
        foreach ($instances[$node_type] as $field) {
          if ($field['widget']['module'] == 'image') {
            $options[$field['field_name']] = t('@human_name (@system_name)', array(
              '@human_name' => $field['label'], '@system_name' => $field['field_name']));
          }
        }
  
        if (!empty($options)) {
          $default_image_field = $edit ? $settings['source']['node']['image_field'] : NULL;
          $form['settings']['source']['wrapper']['node']['image_field'] = array(
            '#type' => 'select',
            '#title' => t('Image field'),
            '#required' => TRUE,
            '#options' => $options,
            '#default_value' => $default_image_field,
          );
        }
      }
    }
  
    if ($source == 'view' && module_exists('views')) {
      // Get all available views
      $views = views_get_all_views();
      $options = array();
      foreach ($views as $view) {
        $options[$view->name] = t('@human_name (@system_name)', array(
          '@human_name' => $view->human_name, '@system_name' => $view->name));
      }
    
      if (!empty($options)) {
        $default_view = $edit ? $settings['source']['view']['view'] : NULL;
        $form['settings']['source']['wrapper']['view']['view'] = array(
          '#type' => 'select',
          '#title' => t('View'),
          '#required' => TRUE,
          '#options' => $options,
          '#default_value' => $default_view,
          '#ajax' => array(
            'callback' => 'bgstretcher_admin_ajax_callback',
            'wrapper' => 'bgstretcher-source-wrapper',
          ),
        );

        if ($edit) {
          $view = $settings['source']['view']['view'];
          if (!empty($form_state['values']['settings']['source']['wrapper']['view']['view'])) {
            $view = $form_state['values']['settings']['source']['wrapper']['view']['view'];
          }
        }
        else {
          $view = $form_state['values']['settings']['source']['wrapper']['view']['view'];
        }
      
        $options = array();
        if (!empty($views[$view]->display)) {
          foreach ($views[$view]->display as $display => $data) {
            if (!empty($data->display_options['fields'])) {
              $options[$display] = check_plain($data->display_title);
            }
          }
        }

        if (!empty($options)) {
  
          $default_display = $edit ? $settings['source']['view']['display'] : NULL;
          $form['settings']['source']['wrapper']['view']['display'] = array(
            '#type' => 'select',
            '#title' => t('Display'),
            '#required' => TRUE,
            '#options' => $options,
            '#default_value' => $default_display,
            '#ajax' => array(
              'callback' => 'bgstretcher_admin_ajax_callback',
              'wrapper' => 'bgstretcher-source-wrapper',
            ),
          );
        }
    
        if ($edit) {
          if (!empty($form_state['values']['settings']['source']['wrapper']['view']['display'])) {
            $display = $form_state['values']['settings']['source']['wrapper']['view']['display'];
          }
          else {
            $display = $settings['source']['view']['display'];
          }
        }
        else {
          $display = $form_state['values']['settings']['source']['wrapper']['view']['display'];
        }
      
        $options = array();
        if (!empty($views[$view]->display[$display]->display_options['fields'])) {
          foreach ($views[$view]->display[$display]->display_options['fields'] as $field => $data) {
            $field_info = field_info_field($data['field']);
            if ($field_info['type'] == 'image') {
              $options[$field] = t('@human_name (@system_name)', array(
                '@human_name' => $data['label'], '@system_name' => $field));
            }
          }
        }
      
        if (!empty($options)) {
          $default_value = $edit ? $settings['source']['view']['image_field'] : NULL;
          $form['settings']['source']['wrapper']['view']['image_field'] = array(
            '#type' => 'select',
            '#title' => t('Image field'),
            '#required' => TRUE,
            '#options' => $options,
            '#default_value' => $default_value,
            '#ajax' => array(
              'callback' => 'bgstretcher_admin_ajax_callback',
              'wrapper' => 'bgstretcher-source-wrapper',
            ),
          );
        }
      }
    }
  }
  
  /* ============================= Settings for Jquery Bgstretcher plugin =================================*/
  $form['settings']['js']['additional_settings']['dimension'] = array(
    '#type' => 'select',
    '#title' => t('Original image dimension'),
    '#default_value' => $edit ? $settings_js['additional_settings']['dimension'] : 'fixed',
    '#options' => array(
      'fixed' => t('Fixed value'),
      'min' => t('Min value'),
      'max' => t('Max value'),
      'average' => t('Average value'),
    ),
  );
  
  $form['settings']['js']['additional_settings']['image_style'] = array(
    '#type' => 'select',
    '#title' => t('Use image style'),
    '#default_value' => $edit ? $settings_js['additional_settings']['image_style'] : '',
    '#options' => image_style_options(),
  );
  
  $form['settings']['js']['additional_settings']['h'] = array(
    '#markup' => '<h3>' . t('Main JS settings') . '</h3>',
  );
    
  $form['settings']['js']['resizeProportionally'] = array(
    '#type' => 'checkbox',
    '#title' => t('Resize proportionally'),
    '#default_value' => $edit ? $settings_js['resizeProportionally'] : 1,
    '#description' => t('Indicates if background image(s) will be resized proportionally or not.'),
  );
    
  $form['settings']['js']['resizeAnimate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Animated resize'),
    '#default_value' => $edit ? $settings_js['resizeAnimate'] : 0,
    '#description' => t('Indicates if background image(s) will be resized with animation.
      Be careful, this may slow down some PCs if your images are large.'),
  );

  $form['settings']['js']['imageWidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Image width'),
    '#default_value' => $edit ? $settings_js['imageWidth'] : 1024,
    '#description' => t("Original image's width."),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[js][additional_settings][dimension]"]' => array('value' => 'fixed'),
      ),
    ),
  );
    
  $form['settings']['js']['imageHeight'] = array(
    '#type' => 'textfield',
    '#title' => t('Image height'),
    '#default_value' => $edit ? $settings_js['imageHeight'] : 768,
    '#description' => t("Original image's height."),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[js][additional_settings][dimension]"]' => array('value' => 'fixed'),
      ),
    ),
  );

  $form['settings']['js']['maxWidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Max image width'),
    '#default_value' => $edit ? $settings_js['maxWidth'] : 'auto',
    '#description' => t("Maximum image's width."),
  );
    
  $form['settings']['js']['maxHeight'] = array(
    '#type' => 'textfield',
    '#title' => t('Max image height'),
    '#default_value' => $edit ? $settings_js['maxHeight'] : 'auto',
    '#description' => t("Maximum image's height."),
  );
    
  $form['settings']['js']['nextSlideDelay'] = array(
    '#type' => 'textfield',
    '#title' => t('Next slide delay'),
    '#default_value' => $edit ? $settings_js['nextSlideDelay'] : 3000,
    '#description' => t('Numeric value in milliseconds. The parameter sets delay until next slide should start.'),
  );
    
  $form['settings']['js']['slideShowSpeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Show slide speed'),
    '#default_value' => $edit ? $settings_js['slideShowSpeed'] : 'normal',
    '#description' => t("Numeric value in milliseconds or jQuery string value ('fast', 'normal', 'slow').
      The parameter sets the speed of transition between images."),
  );
    
  $form['settings']['js']['slideShow'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show slide'),
    '#default_value' => $edit ? $settings_js['slideShow'] : 1,
    '#description' => t('Allows or disallows slideshow functionality.'),
  );
    
  $form['settings']['js']['transitionEffect'] = array(
    '#type' => 'select',
    '#title' => t('Transition effect'),
    '#options' => array(
      'none' => t('None'),
      'fade' => t('Fade'),
      'simpleSlide' => t('Simple slide'),
      'superSlide' => t('superSlide')),
      '#default_value' => $edit ? $settings_js['transitionEffect'] : 'fade',
      '#description' => t('Transition effect'),
  );
    
  $form['settings']['js']['slideDirection'] = array(
    '#type' => 'textfield',
    '#title' => t('Direction'),
    '#default_value' => $edit ? $settings_js['slideDirection'] : 'N',
    '#description' => t('Slide Diraction: N - north, S - south, W - west, E - East.
      If "Transition Effect" = superSlide use also: NW, NE, SW, SE.'),
  );
    
  $form['settings']['js']['sequenceMode'] = array(
    '#type' => 'select',
    '#title' => t('Sequence Mode'),
    '#options' => array(
      'normal' => t('Normal'),
      'back' => t('Back'),
      'random' => t('Random')),
      '#default_value' => $edit ? $settings_js['sequenceMode'] : 'normal',
  );
    
  $form['settings']['js']['buttonPrev'] = array(
    '#type' => 'textfield',
    '#title' => t('"Previous" button class'),
    '#default_value' => $edit ? $settings_js['buttonPrev'] : 'empty',
    '#description' => t('Previous button CSS selector'),
  );
    
  $form['settings']['js']['buttonNext'] = array(
    '#type' => 'textfield',
    '#title' => t('"Next" button class'),
    '#default_value' => $edit ? $settings_js['buttonNext'] : 'empty',
    '#description' => t('Next button CSS selector'),
  );
    
  $form['settings']['js']['pagination'] = array(
    '#type' => 'textfield',
    '#title' => t('Pager class'),
    '#default_value' => $edit ? $settings_js['pagination'] : 'empty',
    '#description' => t('CSS selector for pagination'),
  );
    
  $form['settings']['js']['anchoring'] = array(
    '#type' => 'textfield',
    '#title' => t('Anchoring'),
    '#default_value' => $edit ? $settings_js['anchoring'] : 'left top',
    '#description' => t('Anchoring bgStrtcher area regarding window'),
  );
    
  $form['settings']['js']['anchoringImg'] = array(
    '#type' => 'textfield',
    '#title' => t('Anchoring image'),
    '#default_value' => $edit ? $settings_js['anchoringImg'] : 'left top',
    '#description' => t('Anchoring bgStrtcher area regarding window'),
  );
    
  $form['settings']['js']['preloadImg'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preload images'),
    '#default_value' => $edit ? $settings_js['preloadImg'] : 0,
    '#description' => t('For Preload images use true'),
  );
    
  $form['settings']['js']['stratElementIndex'] = array(
    '#type' => 'textfield',
    '#title' => t('Start element index'),
    '#default_value' => $edit ? $settings_js['stratElementIndex'] : 0,
    '#description' => t('Start element index'),
  );
    
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
    '#validate' => array('_bgstretcher_admin_validate'),
  );
  
  return $form;
}

/**
 * Form builder. Allows administrator to manage slides of particular background stretcher.
 */
function bgstretcher_slide_admin($form, &$form_state, $master_id = NULL, $action = NULL, $slide_id = NULL) {
  
  $form['#tree'] = TRUE;
  
  $css = '
    #bgstretcher-slides-table tr.slide-disabled {
      background-color: #FDD;
    }';
  
  $form['#attached']['css'][] = array(
    'data' => $css,
    'type' => 'inline',
  );

  if ($master_id) {
    $slides = bgstretcher_slides_get($master_id, FALSE);
    $bgstretcher = bgstretcher_bgstretcher_get($master_id);
    $source = $bgstretcher['settings']['source']['source'];
    
    if (!empty($slides)) {
      uasort($slides, 'drupal_sort_weight');
      foreach ($slides as $id => $slide) {
        $status = $slide['enabled'];
        switch ($source) {
          case 'node' :
          case 'upload' :
            $variables = array(
              'style_name' => 'thumbnail',
              'path' => $slide['data'][$source]['image_uri'],
              'height' => NULL,
              'width' => NULL,
            );
            $image = theme('image_style', $variables);
          break;
          case 'url' :
            $url = check_url($slide['data']['url']['url']);
            $variables = array(
              'path' => $url,
              'width' => 100,
              'height' => 75,
            );
            $image = theme('image', $variables);
          break;
        }
        $operations = array();
        $title = $status ? t('Disable') : t('Enable');
      
        $operations[] = array(
          'title' => $title,
          'href' => 'admin/structure/bgstretcher/operations',
          'query' => array(
            'action' => $status ? 'disable' : 'enable',
            'id' => $id,
            'subject' => 'slide',
            'token' => drupal_get_token(),
            'destination' => $_GET['q'],
          ),
        );
        
        $operations[] = array(
          'title' => t('Delete'),
          'href' => 'admin/structure/bgstretcher/operations',
          'query' => array(
            'action' => 'delete',
            'id' => $id,
            'subject' => 'slide',
            'token' => drupal_get_token(),
            'destination' => $_GET['q'],
          ),
        );
        
        $form['table'][$id]['enabled'] = array(
          '#type' => 'hidden',
          '#value' => $status,
          '#tree' => FALSE,
        );
        
        $form['table'][$id]['image'] = array(
          '#markup' => $image,
        );
      
        $form['table'][$id]['title'] = array(
          '#type' => 'textfield',
          '#title' => '<span class="element-invisible">' . t('Title') . '</span>',
          '#default_value' => !empty($slide['data'][$slide['source']]['title']) ?  $slide['data'][$slide['source']]['title'] : '',
        );
        
        $form['table'][$id]['weight'] = array(
          '#type' => 'weight', 
          '#title' => '<span class="element-invisible">' . t('Weight') . '</span>',
          '#default_value' => !empty($slide['weight']) ?  $slide['weight'] : 0, 
          '#delta' => 10,
        );
      
        $form['table'][$id]['operations'] = array(
          '#markup' => theme('links', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline')))),
        );
      }
    
      $form['actions'] = array(
        '#type' => 'actions',
        '#tree' => FALSE,
        '#weight' => 5,
      );
      
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
      );
    }

    $form['master_id'] = array(
      '#type' => 'value',
      '#value' => $master_id,
    );
    
    $form['enabled'] = array(
      '#type' => 'value',
      '#value' => 1,
    );

    $form['data'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add slide'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#weight' => 10,
    );
    
    $form['source'] = array(
      '#type' => 'value',
      '#value' => $source,
    );

    switch ($source) {
      case 'upload' : 
        $form['data']['upload']['title'] = array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#title-display' => 'invisible',
          '#default_value' => '',
        );
        
        $form['data']['upload']['file'] = array(
          '#type' => 'file',
          '#title' => t('Choose a file'),
          '#title_display' => 'invisible',
          '#size' => 22,
          '#theme_wrappers' => array(),
        );
      
        $form['data']['upload']['image_uri'] = array(
          '#type' => 'value',
          '#value' => NULL,
        );
    
    
      break;
      case 'node' : 
        $node_type = $bgstretcher['settings']['source']['node']['type'];
        $form['data']['node']['title'] = array(
          '#type' => 'textfield', 
          '#title' => t('Choose a node by title'),
          '#autocomplete_path' => 'bgstretcher/autocomplete/node/' . $node_type,
        );
  
        $form['data']['node']['nid'] = array(
          '#type' => 'value',
          '#value' => NULL,
        );
        
  
        $form['data']['node']['image_field'] = array(
          '#type' => 'value',
          '#value' => $bgstretcher['settings']['source']['node']['image_field'],
        );
  
        $form['data']['node']['image_uri'] = array(
          '#type' => 'value',
          '#value' => NULL,
        );
      break;
      case 'url' :
        $form['data']['url']['title'] = array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#title-display' => 'invisible',
          '#default_value' => '',
        );
      
        $form['data']['url']['url'] = array(
          '#type' => 'textfield',
          '#title' => t('URL'),
          '#title-display' => 'invisible',
          '#default_value' => '',
        );
      break;  
    }
    
    $form['data']['weight'] = array(
      '#type' => 'weight', 
      '#title' => t('Weight'),
      '#default_value' => '', 
      '#delta' => 10,
      '#tree' => FALSE,
    );

    $form['data']['image_url'] = array(
      '#type' => 'value',
      '#value' => NULL,
    );    
    
    $form['data']['actions'] = array(
      '#type' => 'actions',
      '#tree' => FALSE,
    );
    
    $form['data']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#weight' => 10,
      '#validate' => array('bgstretcher_add_slide_validate'),
      '#submit' => array('bgstretcher_add_slide_submit'),
    );
  }
  return $form;
}

/**
 * Confirmation form. Confirm to delete the given background stretcher's slide
 */
function bgstretcher_slide_delete_confirm($form, &$form_state, $id) {
  
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );

  return confirm_form($form,
    t('Are you sure you want to delete this slide?'),
    $_GET['destination'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

/**
 * Confirmation form. Confirm to delete the given background stretcher
 */
function bgstretcher_delete_confirm($form, &$form_state, $id) {
  
  $bgstretcher = bgstretcher_bgstretcher_get($id);
  $name = $bgstretcher['name'];
  
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %name and all its slides?', array('%name' => $name)),
    'admin/structure/bgstretcher',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

/**
 * Submit callback. Delete specific bgstretcher
 */
function bgstretcher_delete_confirm_submit($form, &$form_state) {
  $id = $form_state['values']['id'];
  if ($form_state['values']['confirm'] && $id) {
    if (bgstretcher_bgstretcher_delete($form_state['values']['id'])) {
      if ($slides = bgstretcher_slides_get($id)) {
        foreach ($slides as $slide_id => $slide) {
          bgstretcher_slide_delete($slide_id);
        }
      }
      drupal_set_message(t('%name deleted', array('%name' => $form_state['values']['name'])));
    }
  }
  $form_state['redirect'] = 'admin/structure/bgstretcher';
}

/**
 * Submit callback. Delete specific bgstretcher's slide
 */
function bgstretcher_slide_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] && $form_state['values']['id']) {
    bgstretcher_slide_delete($form_state['values']['id']);
  }
}

/**
 * Validate the "add slide" form
 */
function bgstretcher_add_slide_validate($form, &$form_state) {
  
  $source = $form_state['values']['source'];
  $master_id = $form_state['values']['master_id'];
  unset($form_state['values']['table']);
  $bgstretcher = bgstretcher_bgstretcher_get($master_id);
  $settings = $bgstretcher['settings']['js']['additional_settings'];

  switch ($source) {
    case 'node' :
      if (!drupal_strlen($form_state['values']['data']['node']['title'])) {
        form_set_error('data][node][title', t('Please choose a valid node'));
        return;
      }

      $title = $form_state['values']['data']['node']['title'];
      $matches = array();
      $nid = 0;
      
      // This preg_match() looks for the last pattern like [33334] and if found extracts the numeric portion.
      $result = preg_match('/\[([0-9]+)\]$/', $title, $matches);
      
      if (!$result) {
        form_set_error('data][node][title', t('Please choose a valid node'));
        return;
      }
  
      if ($result > 0) {
        $nid = $matches[$result];
        $slides = bgstretcher_slides_get($master_id, FALSE);

        if (!empty($slides)) {
          foreach ($slides as $slide) {
            if ($slide['data']['node']['nid'] == $nid) {
              form_set_error('data][node][title', 'This node ID already exists in the stretcher');
              return;
            }
          }
        }

        // Verify that it's a valid nid.
        $node = node_load($nid);
        if (empty($node)) {
          form_set_error('data][node][title', t('No node with nid %nid can be found', array('%nid' => $nid)));
          return;
        }
  
        $image_field = $form_state['values']['data']['node']['image_field'];
        $item = reset(field_get_items('node', $node, $image_field));
        $image_uri = $item['uri'];
  
        form_set_value($form['data']['node']['title'], $node->title, $form_state);
        form_set_value($form['data']['node']['nid'], $nid, $form_state);
        form_set_value($form['data']['node']['image_uri'], $image_uri, $form_state);

        if ($settings['image_style']) {
          $image_url = image_style_url($settings['image_style'], $image_uri);
        }
        else {
          $image_url = file_create_url($image_uri);
        }
        form_set_value($form['data']['image_url'], $image_url, $form_state);
      }

    break;
    case 'upload' :
      $file = file_save_upload('data', array(
        'file_validate_is_image' => array(), // Validates file is really an image. 
        'file_validate_extensions' => array('png gif jpg jpeg'), // Validate extensions.
      ));

      if ($file) {
        // Move the file into the Drupal file system
        if ($file = file_move($file, BGSTRETCHER_UPLOAD_DIRECTORY)) {
          $file->status = FILE_STATUS_PERMANENT;
          $file = file_save($file);
          form_set_value($form['data'][$source]['image_uri'], $file->uri, $form_state);
  
          if ($settings['image_style']) {
            $image_url = image_style_url($settings['image_style'], $file->uri);
          }
          else {
            $image_url = file_create_url($file->uri);
          }
          form_set_value($form['data']['image_url'], $image_url, $form_state);
        }
        else {
          form_set_error('data][upload][file', t('Failed to write the uploaded file the site\'s file folder.'));
        }
      }
      else {
        form_set_error('data][upload][file', t('No file was uploaded.'));
      }
    break;
    case 'url' :
      $url = $form_state['values']['data']['url']['url'];
      if (drupal_strlen($url) != drupal_strlen(drupal_strip_dangerous_protocols($url))) {
        form_set_error('data][url][url', t('Wrong URL format.'));
        return;
      }

      if (!valid_url($url, $absolute = TRUE) || !valid_url($url, $absolute = FALSE)) {
        form_set_error('data][url][url', t('Wrong URL format.'));
        return;
      }
    
      form_set_value($form['data']['image_url'], $url, $form_state);
    break;
  }
  form_set_value($form['data'], serialize($form_state['values']['data']), $form_state);
}

/**
 * Submit callback. Add new background stretcher
 */
function bgstretcher_add_slide_submit($form, &$form_state) {
  // Clean up submitted values
  form_state_values_clean($form_state);
  if (bgstretcher_slide_set($form_state['values'])) {
    drupal_set_message(t('The configuration options have been saved.'));
  }
}

/**
 * Submit callback. Edit the given slide
 */
function bgstretcher_slide_admin_submit($form, &$form_state) {
  // Clean up submitted values
  form_state_values_clean($form_state);
  
  $values = $form_state['values']['table'];
  $master_id = $form_state['values']['master_id'];
  $source = $form_state['values']['source'];
  
  // Get all available slides that belong to the given background stretcher (if any)
  $slides = bgstretcher_slides_get($master_id, FALSE);

  if (!empty($values)) {
    foreach ($values as $slide_id => $fields) {
      $title = $fields['title'];
      unset($fields['title']);
      
      $existing_data = $slides[$slide_id]['data'];
      $existing_data[$source]['title'] = $title;
      $fields['data'] = serialize($existing_data);
      
      if (bgstretcher_slide_set($fields, $slide_id)) {
        drupal_set_message(t('The configuration options have been saved.'));
      }
    }
  }
}

/**
 * Validate new slide
 */
function _bgstretcher_admin_validate($form, &$form_state) {
  $source = $form_state['values']['settings']['source']['wrapper']['source'];
  $id = $form_state['values']['id'];

  if (($source == 'node' || $source == 'view') &&
    empty($form_state['values']['settings']['source']['wrapper'][$source]['image_field'])) {
      form_set_error('', t('Please choose an image field.'));
    return;
  }

  form_set_value($form['settings']['source'], $form_state['values']['settings']['source']['wrapper'], $form_state);
  
  // Kick out unnecessary junk
  unset($form_state['values']['settings']['source']['wrapper']);
  unset($form_state['values']['settings']['settings__active_tab']);  
  
  if ($id && $source != 'url') {
    $image_style = $form_state['values']['settings']['js']['additional_settings']['image_style'];
  
    if ($slides = bgstretcher_slides_get($id)) {
      foreach ($slides as $slide_id => $slide) {
      
        $data = $slide['data'];
        $image_uri = $data[$slide['source']]['image_uri'];
        $image_url = $image_style ? image_style_url($image_style, $image_uri) : file_create_url($image_uri);
        $data['image_url'] = $image_url;
        $fields['data'] = serialize($data);
        bgstretcher_slide_set($fields, $slide_id);
      }
    }
  }
  form_set_value($form['settings'], serialize($form_state['values']['settings']), $form_state);
}

/**
 * Submit callback. Add/edit the given bgstretcher
 */
function bgstretcher_admin_submit($form, &$form_state) {

  form_state_values_clean($form_state);
  $id = $form_state['values']['id'];
  unset($form_state['values']['id']);

  if (bgstretcher_bgstretcher_set($id, $form_state['values'])) {
    drupal_set_message(t('The configuration options have been saved.'));
    $form_state['redirect'] = 'admin/structure/bgstretcher';
  } 
}

/**
 * AJAX callback
 */
function bgstretcher_admin_ajax_callback($form, &$form_state) {
  return $form['settings']['source']['wrapper'];
}
