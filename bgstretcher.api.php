<?php

/**
 * @file
 * Bgstretcher hook definitions.
 */

/**
 * Alter Background slideshow before output.
 *
 * @param $bgstretcher
 *  Bgstretcher array.
 */
function hook_bgstretcher_alter(&$bgstretcher) {
 // Your code here
}
