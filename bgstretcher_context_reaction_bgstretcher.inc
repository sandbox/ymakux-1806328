<?php

/**
 * @file
 * Contains bgstretcher_context_reaction_bgstretcher class. 
 */

/**
 * Expose background stretchers as context reactions.
 */
class bgstretcher_context_reaction_bgstretcher extends context_reaction {
    
  // Plugin settings form  
  function options_form($context) {
    $options = array();

    if ($bgstretchers = bgstretcher_bgstretcher_get(NULL, TRUE)) {
      foreach ($bgstretchers as $id => $bgstretcher) {
        $options[$id] = $bgstretcher['name'];
      }
    }

    $values = $this->fetch_from_context($context);
    $form = array(
      '#tree' => TRUE,
      '#title' => check_plain($this->title),
      'bgstretcher' => array(
        '#title' => check_plain($this->title),
        '#description' => t('Which background stretcher to show when condition is met'),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => isset($values['bgstretcher']) ? $values['bgstretcher'] : '',
      ),
    );
    return $form;
  }    // Performs action when context conditions met
  
  function execute() {
    foreach (context_active_contexts() as $context) {
      if (!empty($context->reactions[$this->plugin]['bgstretcher'])) {
        $id = $context->reactions[$this->plugin]['bgstretcher'];
        if ($bgstretcher = bgstretcher_bgstretcher_get($id, TRUE)) {
          bgstretcher_bgstretcher_prepare($bgstretcher);
          if (!bgstretcher_bgstretcher_is_empty($bgstretcher)) {
            bgstretcher_include_files($bgstretcher);
          }
        }
      }
    }
  }
}
