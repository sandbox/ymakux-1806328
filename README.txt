BgStretcher is a Jquery full-screen background slideshow
Plugin's project page: http://www.ajaxblender.com/bgstretcher-2-jquery-stretch-background-plugin-updated.html

This module allows for integration of BgStretcher plugin into your Drupal site.

Depencies
------------
 - Libraries API 2.x (http://drupal.org/project/libraries)

Installation
------------

 - Download and unpack the module to your module directory.
 - Download the latest version of BgStretcher Jquery Plugin from 
   http://www.ajaxblender.com/bgstretcher-2-jquery-stretch-background-plugin-updated.html
 - Extract from downloaded archive two files - bgstretcher.js and bgstretcher.css and place them into sites/all/libraries/bgstretcher
 - Enable the module on the admin modules page
 - Go to admin/structure/bgstretcher and add a new background stretcher.
  -- Required fields:
    --- "Name". A human name for administrator
    --- "CSS ID". Where to load your background stretcher. You have to use regular CSS selectors.
        For example: body (whole page) or #page (<div id="page">) or .page (<div class="page">)
 - Configure slideshow's visibility, slide source and other settings available under vertical tabs.
 - Go to admin/structure/bgstretcher/slide/<slideshow ID> and add some images to display in the slideshow.
 
For each background slideshow you'll have a corresponding block (see admin/structure/block) that represents the slideshow's pager.

Author
------
Iurii Makukh
ymakux@gmail.com
